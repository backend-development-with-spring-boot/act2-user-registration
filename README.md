#### Day 1 > Activity 2

## User Registration (services/singleton)

1. Create a class `UserService` with a method `register(UserRegistration)`
   1. Move user registration implementation in the new method
   2. `register(UserRegistration)` returns the saved `User`
   3. `register(UserRegistration)` should throw a `InvalidUserRegistrationException` when a validation wasn't met
2. Convert `UserService` as a singleton.
3. Create a method `getNumberOfRegisteredUser()` in `UserService` which will return an `int` value of the # of registered users
   1. To demo, call `register(UserRegistration)` twice, `getNumberOfRegisteredUser()` should return 2
4. Create a class `IdService` with methods `getNextUserId()` and `generateCode(int)`
   1. `getNextUserId()` returns a string
   2. `generateCode(int)` requires `int` as the length of the code and returns a string
   3. Use methods in creating user id and verification code.
   4. Make `IdService` singleton
   5. Add constructor in `UserService` that expects a `IdService`
   6. Use `IdService` methods in generating user id and verification code
5. Create a class `VerificationService` with `saveVerificationCode()`
   1. Class has its own map
   2. Create `getEmailByCode(code)` that returns the email of a given code.s
6. Display statistics
   1. Log current number of registered user right before execution ends.
   
NOTE:
- Use logger
- Log exceptions