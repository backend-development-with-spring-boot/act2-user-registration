package ph.apper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        // Prepare user details to be registered
        UserRegistration userRegistration = new UserRegistration();
        userRegistration.setEmail("jdoe@apper.ph");
        userRegistration.setFirstName("John");
        userRegistration.setLastName("Doe");
        userRegistration.setPassword("strongpassword");
        userRegistration.setBirthDate(LocalDate.of(2000, Month.DECEMBER, 3));

        // validate inputs. all fields are required. Valid email. age should be at least 18 years.
        if (userRegistration.getEmail().isBlank()
                || userRegistration.getFirstName().isBlank()
                || userRegistration.getLastName().isBlank()
                || userRegistration.getPassword().isEmpty()
                || Objects.isNull(userRegistration.getBirthDate())
        ) {
            LOGGER.error("All fields are required!");
            return;// end of program
        }

        if (!userRegistration.getEmail().contains("@")) {
            LOGGER.error("Email {} is invalid", userRegistration.getEmail());
            return;// end of program
        }

        if (userRegistration.getPassword().length() < 5) {
            LOGGER.error("Password must at least 5 characters");
            return;// end of program
        }

        Period periodDiff = Period.between(userRegistration.getBirthDate(), LocalDate.now());
        if (periodDiff.getYears() < 18) {
            LOGGER.error("Age must at least be 18");
            return;// end of program
        }

        // get user id
        String userId = UUID.randomUUID().toString();
        LOGGER.info("Generated User ID: {}", userId);

        // save registration details as User with ID
        User newUser = new User(userId);
        newUser.setFirstName(userRegistration.getFirstName());
        newUser.setLastName(userRegistration.getLastName());
        newUser.setBirthDate(userRegistration.getBirthDate());
        newUser.setEmail(userRegistration.getEmail());
        newUser.setPassword(userRegistration.getPassword());

        // create verification code
        String verificationCode = "123qwe";
        Map<String, String> verificationCodes = new HashMap<>();
        verificationCodes.put(newUser.getEmail(), verificationCode);

        List<User> users = new ArrayList<>();
        users.add(newUser);

        // send verification code to the user
        LOGGER.info("Sending verification code to {} ...", newUser.getEmail());
        LOGGER.info("Verification sent!");

        LOGGER.info("User {} saved!", newUser.getEmail());
        LOGGER.info("Current number of registered users: {}", users.size());
    }
}
